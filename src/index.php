<?php
// SPDX-License-Identifier: AGPL-3.0-or-later

header("Content-Type: text/html; charset=utf-8");
require_once("config.inc.php");

if (!@$_GET["show_results"]) {
    define("APP_ROUTE", "vote");
    require("templates/head.php");
    require("routes/vote.php");
} else {
    define("APP_ROUTE", "results");
    require("templates/head.php");
    require("routes/results.php");
}
require("templates/tail.php");
