<?php
// SPDX-License-Identifier: AGPL-3.0-or-later

header("Content-Type: text/html; charset=utf-8");
require_once("../config.inc.php");

define("APP_ROUTE", "admin");
//require("../templates/head.php"); // TODO: still included in route due to Location header :/
require("../routes/admin.php");
require("../templates/tail.php");
