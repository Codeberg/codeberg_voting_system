<?php
// SPDX-License-Identifier: AGPL-3.0-or-later
require("config.inc.local.php");
const DBUSER = "voting";
const DBNAME = "voting";

if (!isset($mysql_server)) {
    $mysql_server = null;
}

$mysqli = mysqli_connect($mysql_server, DBUSER, DBPASSWD, DBNAME);
mysqli_query($mysqli, "CREATE TABLE IF NOT EXISTS polls (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, poll_start INT NOT NULL, poll_end INT, question TEXT, selectable_answers INT NOT NULL DEFAULT 1)");
mysqli_query($mysqli, "CREATE TABLE IF NOT EXISTS polls_answers (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, poll_id INT NOT NULL, answer VARCHAR(255),votes INT NOT NULL, INDEX index_pollid(poll_id))");
mysqli_query($mysqli, "CREATE TABLE IF NOT EXISTS polls_tokens (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, poll_id INT NOT NULL, token VARCHAR(255), used TIMESTAMP NULL,INDEX index_pollid(poll_id))");
mysqli_set_charset($mysqli, "utf8");

if (!isset($mail_from)) {
    $mail_from = "codeberg@codeberg.org";
}

if (!isset($polls_url)) {
    $polls_url = "https://polls.codeberg.org/";
}
if (!isset($polls_url_short)) {
    $polls_url_short = "polls.codeberg.org";
}
