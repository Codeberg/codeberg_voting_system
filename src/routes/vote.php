<?php
// SPDX-License-Identifier: AGPL-3.0-or-later

$action = @$_POST["action"];
if ($action == "vote") {
	$success = false;
	$input_token = @$_POST["input_token"];
	$poll_id = @$_POST["poll_id"];
	$answer_ids = @$_POST["answer_id"];
	if (!$answer_ids) {
		$answer_ids = [];
	}
	$answer_ids = array_unique($answer_ids);
	$valid = check_valid_vote($poll_id, $answer_ids, $input_token);
	if ($valid !== true) {
		$message = $valid;
	} else {
		$stmt = $mysqli->prepare("UPDATE polls_tokens SET used=now() WHERE poll_id=? AND token=? AND used IS NULL");
		$stmt->bind_param("is", $poll_id, $input_token);
		$stmt->execute();

		if ($mysqli->affected_rows == 1) {
			unset($stmt);
			foreach ($answer_ids as $answer_id) {
				$answer_id = intval($answer_id);
				$stmt = $mysqli->prepare("UPDATE polls_answers SET votes=votes+1 WHERE id=? AND poll_id=?");
				$stmt->bind_param("ii", $answer_id, $poll_id);
				$stmt->execute();
				unset($stmt);
			}

			$success = true;
			$message = "Thank you for your vote!";
		} else {
			$message = "This token has already been used!";
		}
	}
	$color = $success ? "success" : "danger";
	echo <<<EOF
<div class="container mb-10">
  <div class="alert alert-$color filled" role="alert">
    <!--<h4 class="alert-heading">-->$message<!--</h4>-->
  </div>
</div>
EOF;
}
?>

<div class="container pt-20" id="voting-columns">
	<style>
		@media screen and (min-width: 769px) {
			#voting-columns {
				column-count: 2;
				column-gap: 50px;
			}
		}

		#voting-columns>* {
			break-inside: avoid;
		}
	</style>
	<?php
	$time = time();
	$mintime = time() - 365 * 24 * 60 * 60;
	$result = $mysqli->query("SELECT * FROM polls WHERE poll_start<=$time AND poll_end>$time ORDER BY poll_start desc;");
	while ($row = $result->fetch_assoc()) {
		$poll_id = $row["id"];
		$selectable_answers = $row["selectable_answers"];
		$question = nl2br($row["question"]);
		$until = date("Y-m-d H:i:s", $row["poll_end"]);

		echo <<<EOF
<div class="w-full">
  <div class="card mt-0 mx-0">
    <h2 class="card-title">
      $question
      <div class="text-muted font-size-14 font-weight-normal mt-5">
        <i class="fa fa-hourglass-half"></i>
        Active until: $until UTC
      </div>
    </h2>
EOF;

		if ($selectable_answers > 1) {
			echo "<p class=\"text-muted\">Please select up to $selectable_answers answers!</p>";
		}

		echo <<<EOF
    <!--<p class="text-muted">
      Subtitle that hooks the reader and prompts them to click on the read more button.
    </p>-->
    <form method="POST">
      <input type="hidden" name="poll_id" value="$poll_id">
      <div class="form-group">
EOF;

		$result2 = $mysqli->query("SELECT * FROM polls_answers WHERE poll_id=$poll_id");
		$i = 0;
		while ($row2 = $result2->fetch_assoc()) {
			$i++;
			if ($selectable_answers > 1) {
				echo "<div class=\"custom-checkbox\"><input type=\"checkbox\" name=\"answer_id[]\" id=\"answer-{$poll_id}-{$i}\" value=\"{$row2["id"]}\"><label for=\"answer-$poll_id-$i\">{$row2["answer"]}</label></div>";
			} else {
				echo "<div class=\"custom-radio\"><input type=\"radio\" name=\"answer_id[]\" id=\"answer-{$poll_id}-{$i}\" value=\"{$row2["id"]}\"><label for=\"answer-$poll_id-$i\">{$row2["answer"]}</label></div>";
			}
		}

		echo <<<EOF
      </div>
      <div class="form-group form-inline mb-0">
        <label for="full-name" class="required">Voting Token:</label>
        <input name="input_token" type="text" class="form-control">
        <button class="btn btn-primary" type="submit" name="action" value="vote">Vote now!</button>
      </div>
    </form>
  </div>
</div>
EOF;
	}

	function check_valid_vote($poll_id, $answer_ids, $token)
	{
		global $mysqli;

		if (!$token)
			return "no token supplied";

		if (count($answer_ids) < 1)
			return "no option selected";

		if (!$poll_id)
			return "no poll_id supplied";

		foreach ($answer_ids as $answer_id) {
			$answer_id = intval($answer_id);
			$stmt = $mysqli->prepare("SELECT id FROM polls_answers WHERE (id=? AND poll_id=?)");
			$stmt->bind_param("ii", $answer_id, $poll_id);
			$stmt->bind_result($exists_in_answers);
			$stmt->execute();
			$stmt->fetch();
			if (!$exists_in_answers) {
				return "selected option is not valid or part of another poll";
			}
			unset($stmt);
		}
		$stmt = $mysqli->prepare("SELECT id FROM polls_tokens WHERE poll_id=? AND token=?");
		$stmt->bind_param("is", $poll_id, $token);
		$stmt->bind_result($exists_in_tokens);
		$stmt->execute();
		$stmt->fetch();
		if (!$exists_in_tokens) {
			return "token is not valid or part of another poll";
		}

		unset($stmt);
		$stmt = $mysqli->prepare("SELECT poll_start, poll_end, selectable_answers FROM polls WHERE id=?");
		$stmt->bind_param("i", $poll_id);
		$stmt->bind_result($poll_start, $poll_end, $selectable_answers);
		$stmt->execute();
		$stmt->fetch();
		$time = time();

		if (count($answer_ids) > $selectable_answers) {
			return "you selected more than $selectable_answers";
		}
		if ($time > $poll_end) {
			return "poll has already ended";
		}
		if ($time < $poll_start) {
			return "poll has not yet started";
		}

		return true;
	}

	?>
</div>