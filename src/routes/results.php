<?php
// SPDX-License-Identifier: AGPL-3.0-or-later
?>

<div class="container pt-20" id="voting-columns">
	<style>
		@media screen and (min-width: 769px) {
			#voting-columns {
				column-count: 2;
				column-gap: 50px;
			}
		}

		#voting-columns>* {
			break-inside: avoid;
		}
	</style>
	<?php
	$time = time();
	$mintime = time() - 365 * 24 * 60 * 60;

	$result = $mysqli->query("SELECT * FROM polls WHERE poll_end<$time and poll_end>$mintime order by poll_end desc");
	if ($result->num_rows) {
		while ($row = $result->fetch_assoc()) {
			$poll_id = $row["id"];
			$question = $row["question"];
			show_results($poll_id, $question, $row["poll_end"]);
		}
	}

	function show_results($poll_id, $question, $until)
	{
		global $mysqli;

		$question = nl2br($question);
		$until = date("Y-m-d H:i:s", $until);
		echo <<<EOF
<div class="w-full">
  <div class="card mt-0 mx-0">
    <h2 class="card-title">
      $question
      <div class="text-muted font-size-14 font-weight-normal mt-5">
        Completed since: $until UTC
      </div>
    </h2>
EOF;

		$stmt = $mysqli->prepare("SELECT COUNT(*) FROM polls_tokens WHERE poll_id=? and used>0");
		$stmt->bind_param("i", $poll_id);
		$stmt->bind_result($votecount);
		$stmt->execute();
		$stmt->fetch();
		unset($stmt);

		$stmt = $mysqli->prepare("SELECT answer,votes FROM polls_answers WHERE poll_id=?");
		$stmt->bind_param("i", $poll_id);
		$stmt->bind_result($answer, $votes);
		$stmt->execute();

		while ($stmt->fetch()) {
			echo "<p class=\"mb-0\">$answer <small>($votes votes)</small></p>";

			$pct = 0;
			if ($votecount > 0) {
				$pct = round(($votes / $votecount) * 100);
			}
			echo "<div class=\"progress\"><div class=\"progress-bar\" role=\"progressbar\" style=\"width: $pct%\" aria-valuenow=\"$pct\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div></div>";
		}

		echo <<<EOF
  </div>
</div>
EOF;
	}
	?>
</div>