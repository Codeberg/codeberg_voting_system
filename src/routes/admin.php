<?php
// SPDX-License-Identifier: AGPL-3.0-or-later

$maxanswers = 10;
$max_selectable_answers = 5;

$action = @$_POST["action"];
if ($action == "addpoll") {
	$question = $_POST["input_question"];
	$voters = $_POST["input_voters"];
	$voters = explode("\n", $voters);
	$valid_from = strtotime($_POST["input_valid_from"]);
	$valid_till = strtotime($_POST["input_valid_till"]);
	$selectable_answers = intval($_POST["selectable_answers"]);
	if (($selectable_answers > $max_selectable_answers) || ($selectable_answers < 1)) {
		echo "error, seleactable answers must be between 1 and $max_selectable_answers";
	}

	if ($question && $valid_from && $valid_till && count($voters)) {
		$stmt = $mysqli->prepare("INSERT INTO polls (question,poll_start,poll_end,selectable_answers) VALUES (?,?,?,?)");
		insert_poll($stmt, $question, $valid_from, $valid_till, $selectable_answers);
		$poll_id = $mysqli->insert_id;
		$stmt = $mysqli->prepare("INSERT INTO polls_answers (poll_id, answer, votes) VALUES (?,?,0)");

		$i = 1;
		$answer = @$_POST["input_answer_$i"];
		while ($answer) {
			insert_answer($stmt, $poll_id, $answer);
			$i++;
			$answer = @$_POST["input_answer_$i"];
		}

		$headers = "From: $mail_from\r\n" .
			"MIME-Version: 1.0\r\n" .
			"Content-type: text/plain; charset=utf-8";

		$mailtext = "A new poll has been created on $polls_url_short:\n\n" .
			"Poll Question:\n\n" . $question . "\n\n" .
			"Plese vote with token TOKEN here: $polls_url" . "\n\n" .
			"This vote is completely anonymous, voting slips are not stored, only a counter is incremented, your token is not associated with you or your IP address in any way and neither this mail nor your token when voting will be written to any log file.\n\n" .
			"If your browser asks for login credentials please use the ones from the URL above. (the part between https:// and @).\n\n" .
			"The voting tokens are valid for 14 days\n" .
			"\n" .
			"--\n" .
			"https://codeberg.org\n" .
			"Codeberg e.V.  –  Arminiusstraße 2-4  –  10551 Berlin  –  Germany\n";
		"Registered at registration court Amtsgericht Charlottenburg VR36929.\n";

		$stmt = $mysqli->prepare("INSERT INTO polls_tokens (poll_id,token,used) VALUES (?,?,NULL)");
		foreach ($voters as $voter) {
			$voter = trim($voter);
			if (!$voter) continue;
			$token = generateCode();
			insert_token($stmt, $poll_id, $token);
			$thismailtext = str_replace("TOKEN", $token, $mailtext);
			mail_better_privacy($voter, "New poll on $polls_url_short", $thismailtext, $headers);
		}
		header("Location:" . $_SERVER["PHP_SELF"]);
		exit;
	}
} else if ($action == "delete_poll") {
	$poll_id = $_POST["poll_id"];
	delete_poll($poll_id);
}
require("../templates/head.php");
?>
<div class="container">
	<h2 class="content-title">Create new poll</h2>

	<form method="POST" class="form-inline w-full">
		<div class="form-group">
			<label class="required w-100" for="input_question"><strong>Question:</strong></label>
			<textarea class="form-control" name="input_question" id="input_question" required></textarea>
		</div>

		<?php
		for ($i = 1; $i <= $maxanswers; $i++) {
			echo "<div class=\"form-group mb-5\">";
			if ($i == 1) {
				echo "<label style=\"width: 80px;\" class=\"required mr-auto\" for=\"input_answer_1\"><strong>Options:</strong></label>";
				echo "<label style=\"width: 20px;\" class=\"justify-content-end\" for=\"input_answer_$i\">$i</label>";
			} else {
				echo "<label class=\"w-100 justify-content-end\" for=\"input_answer_$i\">$i</label>";
			}
			echo "<input type=\"text\" class=\"form-control\" name=\"input_answer_$i\" id=\"input_answer_$i\">";
			echo "</div>";
		}
		?>
		<div class="form-group mt-15">
			<label class="w-100 required" for="selectable_answers"><strong>Choices:</strong></label>
			<input type="number" class="form-control" style="max-width: 100px;" name="selectable_answers" id="selectable_answers" required value="1" min="1" max="<?= $maxanswers ?>">
		</div>
		<div class="form-group">
			<label class="w-100 required" for="input_valid_from"><strong>Start (UTC):</strong></label>
			<input type="datetime-local" class="form-control" name="input_valid_from" id="input_valid_from" required>
		</div>
		<div class="form-group">
			<label class="w-100 required" for="input_valid_till"><strong>End (UTC):</strong></label>
			<input type="datetime-local" class="form-control" name="input_valid_till" id="input_valid_till" required>
		</div>
		<div class="form-group">
			<label class="w-100 required" for="input_voters"><strong>Voters:</strong></label>
			<div class="flex-grow-1">
				<div class="form-text">Newline-separated list of email addresses:</div>
				<textarea class="form-control" name="input_voters" id="input_voters" required></textarea>
			</div>
		</div>
		<button class="btn btn-primary" type="submit" name="action" value="addpoll">Create poll & send invites!</button>
	</form>
</div>
<div class="container py-20">
	<hr class="my-20">
</div>
<div class="container">
	<h2 class="content-title">Existing polls</h2>
	<?php

	$result = $mysqli->query("select * from polls");
	while ($row = $result->fetch_assoc()) {
		$question = $row["question"];
		$poll_id = $row["id"];
		$poll_end = $row["poll_end"];
		$poll_start = $row["poll_start"];

		echo "<div id='poll_$poll_id' style='margin-bottom:20px;'>";
		echo "<table class='table table-bordered'>";
		echo "<tr>";
		echo "<td>Question:</td>";
		echo <<<EOF
    <td>
        <form method="post" class="float-right ml-10" style="margin-top: -2px; margin-bottom: -2px;">
	        <input type="hidden" name="poll_id" value="$poll_id">
            <button class="btn btn-danger btn-sm" name="action" value="delete_poll">Delete</button>
	    </form>
        <strong>$question</strong>
    </td>
EOF;

		$count = 0;
		echo "</tr>";
		$stmt = $mysqli->prepare("select answer from polls_answers where poll_id=?");

		if (
			$stmt &&
			$stmt->bind_param('i', $poll_id) &&
			$stmt->execute() &&
			$stmt->store_result() &&
			$stmt->bind_result($answer)
		) {

			while ($stmt->fetch()) {
				$count++;
				echo "<tr>";
				echo "<td width=80>Option $count:</td>";
				echo "<td width=300>$answer</td>";
				echo "</tr>";
			}
		}

		echo "<tr>";
		echo "<td width=80>Poll Start:</td>";
		echo "<td width=300>" . date("Y-m-d H:i:s", $poll_start) . "</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td width=80>Poll End:</td>";
		echo "<td width=300>" . date("Y-m-d H:i:s", $poll_end) . "</td>";
		echo "</tr>";

		echo "</table>";
		echo "</div>";
	}

	echo "</div>";

	function insert_poll($stmt, $question, $valid_from, $valid_till, $selectable_answers)
	{
		if (
			$stmt &&
			$stmt->bind_param('siii', $question, $valid_from, $valid_till, $selectable_answers) &&
			$stmt->execute()
		) {
			return true;
		}
		return false;
	}

	function insert_answer($stmt, $poll_id, $answer)
	{
		if (
			$stmt &&
			$stmt->bind_param('is', $poll_id, $answer) &&
			$stmt->execute()
		) {
			return true;
		}
		return false;
	}

	function insert_token($stmt, $poll_id, $token)
	{
		if (
			$stmt &&
			$stmt->bind_param('is', $poll_id, $token) &&
			$stmt->execute()
		) {
			return true;
		}
		return false;
	}

	function delete_poll($poll_id)
	{
		global $mysqli;
		$stmt = $mysqli->prepare("delete from polls where id=?");
		$stmt->bind_param('i', $poll_id);
		$stmt->execute();
		$stmt = $mysqli->prepare("delete from polls_answers where poll_id=?");
		$stmt->bind_param('i', $poll_id);
		$stmt->execute();
		$stmt = $mysqli->prepare("delete from polls_tokens where poll_id=?");
		$stmt->bind_param('i', $poll_id);
		$stmt->execute();
	}

	function generateCode($length = 40)
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789";
		$count = strlen($chars);

		for ($i = 0, $result = ''; $i < $length; $i++) {
			$index = rand(0, $count - 1);
			$result .= substr($chars, $index, 1);
		}

		return $result;
	}

	function mail_better_privacy($to, $subject, $message, $additional_headers)
	{
		$orig_php_self = $_SERVER['PHP_SELF'];
		$orig_remote_addr = $_SERVER['REMOTE_ADDR'];

		$_SERVER['PHP_SELF'] = "/";
		$_SERVER['REMOTE_ADDR'] = $_SERVER['SERVER_ADDR'];

		$ret = mail($to, $subject, $message, $additional_headers);

		$_SERVER['PHP_SELF'] = $orig_php_self;
		$_SERVER['REMOTE_ADDR'] = $orig_remote_addr;

		return $ret;
	}
