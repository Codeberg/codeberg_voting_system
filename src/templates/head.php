<!DOCTYPE html>
<html lang="en" class="codeberg-design">

<head>
    <!-- Meta tags -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta name="viewport" content="width=device-width" />

    <!-- Don't index this example page -->
    <meta name="robots" content="noindex,nofollow">

    <!-- Favicon and title -->
    <link rel="icon" href="https://design.codeberg.org/favicon.ico">
    <title>Codeberg Voting System</title>

    <!-- Codeberg Design CSS -->
    <link rel="icon" href="https://design.codeberg.org/logo-kit/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="https://design.codeberg.org/logo-kit/favicon.svg" type="image/svg+xml" />
    <link rel="apple-touch-icon" href="https://design.codeberg.org/logo-kit/apple-touch-icon.png" />

    <link rel="stylesheet" href="https://design.codeberg.org/design-kit/codeberg.css" />
    <script defer src="https://design.codeberg.org/design-kit/codeberg.js"></script>
    <script defer src="https://design.codeberg.org/components/codeberg-components.js"></script>

    <link href="https://fonts.codeberg.org/dist/inter/Inter%20Web/inter.css" rel="stylesheet" />
    <link href="https://fonts.codeberg.org/dist/fontawesome5/css/all.min.css" rel="stylesheet" />
</head>

<body data-dm-shortcut-enabled="true" data-sidebar-shortcut-enabled="true" data-set-preferred-mode-onload="true">

    <div class="page-wrapper with-navbar">

        <!-- Sticky alerts (toasts), empty container -->
        <!-- Reference: https://www.gethalfmoon.com/docs/sticky-alerts-toasts -->
        <div class="sticky-alerts"></div>

        <!-- Navbar start -->
        <nav class="navbar">
            <!-- Reference: https://www.gethalfmoon.com/docs/navbar -->
            <!-- Navbar brand -->
            <a href="/" class="navbar-brand" title="Codeberg Voting System">
                <img src="https://design.codeberg.org/logo-kit/icon_inverted.svg" alt="Codeberg">
                Voting System
            </a>
            <!-- Navbar nav -->
            <ul class="navbar-nav d-none d-md-flex"> <!-- d-none = display: none, d-md-flex = display: flex on medium screens and up (width > 768px) -->
                <li class="nav-item<?= APP_ROUTE == 'vote' ? ' active' : '' ?>">
                    <a href="?show_results=0" class="nav-link">Current Polls</a>
                </li>
                <li class="nav-item<?= APP_ROUTE == 'results' ? ' active' : '' ?>">
                    <a href="?show_results=1" class="nav-link">Results</a>
                </li>
            </ul>
            <ul class="navbar-nav d-none d-md-flex ml-auto">
                <li class="nav-item<?= APP_ROUTE == 'admin' ? ' active' : '' ?>">
                    <a href="/admin/" class="nav-link" title="Administration">
                        <i class="fa fa-tools"></i>
                    </a>
                </li>
            </ul>
            <!-- Navbar content (with the dropdown menu) -->
            <div class="navbar-content d-md-none ml-auto"> <!-- d-md-none = display: none on medium screens and up (width > 768px), ml-auto = margin-left: auto -->
                <div class="dropdown with-arrow">
                    <button class="btn" data-toggle="dropdown" type="button" id="navbar-dropdown-toggle-btn-1">
                        Menu
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right w-200" aria-labelledby="navbar-dropdown-toggle-btn-1"> <!-- w-200 = width: 20rem (200px) -->
                        <a href="/" class="dropdown-item">Current Votes</a>
                        <!--<a href="/archive/" class="dropdown-item">Codeberg Design Wiki</a>-->
                        <a href="/admin/" class="dropdown-item">Administration</a>
                    </div>
                </div>
            </div>
        </nav>
        <!-- Navbar end -->

        <!-- Content wrapper start -->
        <div class="content-wrapper">
            <!-- Container-fluid -->
            <div class="container-fluid">
                <div class="content">