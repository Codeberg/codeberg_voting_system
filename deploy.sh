#!/bin/bash -ex

HOSTNAME_FQDN=$1
if [ -z ${HOSTNAME_FQDN} ]; then
	echo "no hostname specified"
	exit
fi

DEST_DIR="/var/www/polls"

rsync -av -e ssh --delete --exclude '*~' --exclude config.inc.local.php --chown www-data:www-data ./src/ root@${HOSTNAME_FQDN}:${DEST_DIR}
cat setup_database.sh | ssh root@${HOSTNAME_FQDN} "cd ${DEST_DIR} ; ./setup_database.sh; sh"
