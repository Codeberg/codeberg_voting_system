#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later


PASSWDFILE="src/config.inc.local.php"

if [ -f $PASSWDFILE ]; then
	echo "$PASSWDFILE already exists, won't setup database"
else
	PASSWD=`pwgen 14 1`

	echo "<?php\nconst DBPASSWD='$PASSWD';\n?>" > $PASSWDFILE
	chmod 600 $PASSWDFILE
	mysql -e "CREATE USER 'voting'@'localhost' IDENTIFIED BY '$PASSWD';"
	mysql -e "CREATE DATABASE voting"
	mysql -e "GRANT ALL PRIVILEGES ON voting.* TO 'voting'@'localhost';"

	echo $PASSWD
fi
