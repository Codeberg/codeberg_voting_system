## Codeberg voting system

### Concept

The main idea is maximum privacy and anonymity.

When polls are created, 40 character long tokens are generated and put into
the database and sent out via to all participants via email. A token can only be
used for one poll, multiple polls can run at the same time.

When voting the user has to copy&paste the token to a form that is then
posted. Tokens will be maked as used and the vote counter for the selected
answer will be increased.

No logs will contain anything about the selected answer or the token
connected to it. We also do not know which token was sent to which user.

### Installation
To create the database run ./setup_database.sh and put the random password
into contig.inc.php
